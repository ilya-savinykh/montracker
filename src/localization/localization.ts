import LocalizedStrings from "react-native-localization";
import {errorsLocalization} from "app/localization/errorsLocalization";
import {commonLocalization} from "app/localization/commonLocalization";

class Localization {
  common = new LocalizedStrings(commonLocalization);
  errors = new LocalizedStrings(errorsLocalization);

  getLanguage(): string {
    return this.common.getLanguage();
  }

  getInterfaceLanguage(): string {
    return this.common.getInterfaceLanguage();
  }

  setLanguage(l: string): void {
    this.common.setLanguage(l);
    this.errors.setLanguage(l);
  }
}

export const localization = new Localization();
