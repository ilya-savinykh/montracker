export const commonLocalization = {
  ru: {
    date: 'Дата',
    title: 'Название',
    note: 'Заметка',
    time: 'Время',
    save: 'СОХРАНИТЬ',
    delete: 'УДАЛИТЬ',
    nameUser: 'Ваше имя и фамилия',
    email: 'E-mail',
    company: 'Место работы (компания)',
    developers: 'Команда разработчиков:'
  }
};
