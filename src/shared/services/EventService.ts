import {NavigationActions} from "app/navigation";
import {Store} from "redux";
import {NavigationService} from "app/shared/services/NavigationService";

export class EventService {
  private static store: Store;

  static init(store: Store): void {
    this.store = store;
    this.store.getState(); // for eslint to be silent
  }

  static login(): void {
    // Describe your after-login actions
    NavigationService.dispatch(NavigationActions.navigateToEmptyWithParams({text: "fromLogin"}));
  }
}
