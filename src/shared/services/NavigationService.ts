import {NavigationContainerRef} from "@react-navigation/core";
import { NavigationAction as NativeAction } from "@react-navigation/native";

export class NavigationService {
  private static navigatorRef: NavigationContainerRef;

  static setTopLevelNavigator(navigatorRef: NavigationContainerRef): void {
    this.navigatorRef = navigatorRef;
  }

  static dispatch(action: NativeAction): void {
    if ( this.navigatorRef ) {
      this.navigatorRef.dispatch(action);
    } else {
      console.warn("NavigatorRef is not available");
    }
  }
}
