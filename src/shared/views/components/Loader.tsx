import React, {PureComponent} from "react";
import {ActivityIndicator, Animated, StyleSheet, TextStyle, ViewStyle} from "react-native";
import {Color, fonts, styleSheetCreate, styleSheetFlatten} from "app/shared/helpers";
import {CommonText} from "app/shared/views/components/CommonText";

interface IProps {
  isLoading: boolean;
  transparent?: boolean;
  text?: string;
}

interface IState {
  opacity: Animated.Value;
  isAnimationInProgress: boolean;
}

export class Loader extends PureComponent<IProps, IState> {
  static defaultProps: Partial<IProps>;
  private readonly style: ViewStyle;

  constructor(props: IProps) {
    super(props);

    this.state = {opacity: new Animated.Value(props.isLoading ? 1 : 0), isAnimationInProgress: false};
    this.style = StyleSheet.flatten([styles.indicatorContainer, {opacity: this.state.opacity as any}]);
    if (props.transparent) {
      this.style = styleSheetFlatten([this.style, {backgroundColor: Color.transparent}]) as ViewStyle;
    }
  }

  componentDidUpdate(prevProps: IProps): void {
    if (this.props.isLoading !== prevProps.isLoading) {
      const config: Animated.TimingAnimationConfig = {
        duration: 300,
        toValue: this.props.isLoading ? 1 : 0,
        useNativeDriver: true
      };
      this.setState({isAnimationInProgress: true});
      Animated.timing(
        this.state.opacity, config).start((): void => {
        this.setState({isAnimationInProgress: false});
      }
      );
    }
  }

  render(): JSX.Element | null {
    const {isLoading, text} = this.props;

    if (isLoading || this.state.isAnimationInProgress) {
      return (
        <Animated.View style={this.style}>
          <ActivityIndicator animating={isLoading} size="large"/>
          {this.renderText(text)}
        </Animated.View>
      );
    }

    return null;
  }

  private renderText = (textToShow?: string): JSX.Element | null => {
    if (typeof textToShow === "string") {
      return <CommonText style={styles.text}>{textToShow.toUpperCase()}</CommonText>;
    }

    return null;
  };
}

Loader.defaultProps = {transparent: true};

const styles = styleSheetCreate({
  indicatorContainer: {
    flex: 1,
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: Color.white,
    zIndex: 99
  } as ViewStyle,
  text: {
    alignSelf: "center",
    fontFamily: fonts.medium,
    fontSize: 12,
    color: Color.black,
    paddingTop: 10,
  } as TextStyle,
});
