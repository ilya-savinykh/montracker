import { Color, defaultFontSize, fonts, style, styleSheetCreate, styleSheetFlatten, windowHeight, windowWidth } from 'app/shared/helpers'
import React, { PureComponent } from 'react'
import { View, TextInput, TextStyle, ViewStyle, StyleProp } from 'react-native'
import { CommonText } from './CommonText'

interface IProps {
  title: string
  value: string
  onChangeText(text: string): void
  titleStyle?: TextStyle
  inputStyle?: TextStyle | TextStyle[]
  containerStyle?: ViewStyle
  multiline?: boolean
}

export class CommonInput extends PureComponent<IProps> {
  render () {
    const titleStyle: StyleProp<TextStyle> = styleSheetFlatten([styles.inputTitle, this.props.titleStyle && this.props.titleStyle])
    const inputStyle: StyleProp<TextStyle> = styleSheetFlatten([styles.input, this.props.inputStyle && this.props.inputStyle])
    const containerStyle: StyleProp<TextStyle> = styleSheetFlatten([styles.inputItem, this.props.containerStyle && this.props.containerStyle])
    return (
      <View style={containerStyle}>
        <CommonText family={fonts.light} size={defaultFontSize * .6} color={Color.white} style={titleStyle}>
          {this.props.title}
        </CommonText>
        <TextInput
          value={this.props.value}
          onChangeText={this.props.onChangeText}
          style={inputStyle}
          multiline={this.props.multiline}
        />
      </View>
    )
  }
}

const styles = styleSheetCreate({
  inputItem: style.view({
    marginTop: windowHeight * .03,
  }),
  inputTitle: style.text({
    marginLeft: windowWidth * .06,
  }),
  input: style.text({
    backgroundColor: Color.oceanBlue,
    color: Color.white,
    fontSize: defaultFontSize * .6,
    paddingHorizontal: windowWidth * .06,
    marginTop: windowWidth * .025,
    borderRadius: windowWidth * .0165,
    textDecorationLine: 'none',
    fontFamily: fonts.bold,
  })
})