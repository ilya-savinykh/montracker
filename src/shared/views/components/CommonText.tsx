import {Text, TextProps, TextStyle} from "react-native";
import {Color, defaultFontSize, fonts, style as s, styleSheetCreate, styleSheetFlatten} from "app/shared/helpers";
import React from "react";

interface IProps extends TextProps {
  color?: Color;
  size?: number;
  bold?: boolean;
  family?: string;
  italic?: boolean;
}

export class CommonText extends React.PureComponent<IProps> {
  static defaultProps = {
    color: Color.black,
    size: defaultFontSize,
    bold: false,
    family: fonts.regular,
    italic: false,
  };

  getStyle(): any {
    const {color, size, bold, family, italic} = this.props;
    const style: any = {
      color,
      fontSize: size,
      fontWeight: bold ? "bold" : undefined,
      fontFamily: family,
      fontStyle: italic ? "italic" : "normal",
    } as TextStyle;

    return styleSheetFlatten(
      [styles.default, style],
    );
  }

  render(): JSX.Element {
    const {style, ...props} = this.props;
    const tStyle: any = this.getStyle();

    return (
      <Text
        style={styleSheetFlatten([tStyle, style])}
        allowFontScaling={false}
        {...props}
      />
    );
  }
}

const styles = styleSheetCreate({
  default: s.text({
    fontFamily: fonts.regular,
  }),
});
