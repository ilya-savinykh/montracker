import React from "react";
import {IEmpty} from "app/shared/helpers";

export abstract class ConnectedComponent<TSP, TDP, TOwnProps = IEmpty, TS = IEmpty>
  extends React.Component<IConnectedProps<TSP, TDP, TOwnProps>, TS> {
  get dispatchProps(): TDP {
    return (this.props as any).dispatchProps;
  }

  get stateProps(): TSP {
    return (this.props as any).stateProps;
  }
}

export type IConnectedProps<TStateProps, TDispatchProps, TOwnProps = IEmpty> = {
  [key in keyof TOwnProps]: TOwnProps[key]
} & {
  readonly stateProps?: TStateProps;
  readonly dispatchProps?: TDispatchProps;
};
