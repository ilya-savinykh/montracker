import * as React from "react";
import {PureComponent} from "react";
import {View} from "react-native";
import {Color, defaultFontSize, style, styleSheetCreate} from "app/shared/helpers";
import {CommonText} from "app/shared/views/components/CommonText";
import {RouteProp} from "@react-navigation/native";
import {StackNavigationProp} from "@react-navigation/stack";
import {TInitialStackParams} from "app/navigation/configurations";

interface IProps {
  navigation: StackNavigationProp<any>;
  route: RouteProp<TInitialStackParams, "sEmpty">;
}

export class EmptyScreen extends PureComponent<IProps> {
  render(): JSX.Element {
    return (
      <View style={styles.container}>
        <CommonText
          size={defaultFontSize * .8}
        >
          {this.props.route.params.text}
        </CommonText>
      </View>
    );
  }
}

const styles = styleSheetCreate({
  container: style.view({
    flex: 1,
    backgroundColor: Color.white,
    justifyContent: "center",
    alignItems: "center"
  })
});
