import Axios, {AxiosInstance, AxiosRequestConfig, AxiosResponse} from "axios";

export class BaseClient {
  protected client: AxiosInstance;

  constructor(url: string, config: AxiosRequestConfig = {}) {
    config.baseURL = config.baseURL || url;
    this.client = Axios.create(config);
  }

  setRequestInterceptors(
    onFulfilled?: (value: AxiosRequestConfig) => AxiosRequestConfig | Promise<AxiosRequestConfig>,
    onRejected?: (error: any
    ) => any
  ): void {
    this.client.interceptors.request.use(onFulfilled, onRejected);
  }

  setResponseInterceptors(
    onFulfilled?: (value: AxiosResponse) => AxiosResponse | Promise<AxiosResponse>, onRejected?: (error: any) => any
  ): void {
    this.client.interceptors.response.use(onFulfilled, onRejected);
  }

  async delete<T = any, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R> {
    return this.client.delete(url, config);
  }

  async get<T = any, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R> {
    return this.client.get(url, config);
  }

  async head<T = any, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R> {
    return this.client.head(url, config);
  }

  async options<T = any, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R> {
    return this.client.options(url, config);
  }

  async patch<T = any, R = AxiosResponse<T>>(url: string, data?: any, config?: AxiosRequestConfig): Promise<R> {
    return this.client.patch(url, data, config);
  }

  async post<T = any, R = AxiosResponse<T>>(url: string, data?: any, config?: AxiosRequestConfig): Promise<R> {
    return this.client.post(url, data, config);
  }

  async put<T = any, R = AxiosResponse<T>>(url: string, data?: any, config?: AxiosRequestConfig): Promise<R> {
    return this.client.put(url, data, config);
  }
}
