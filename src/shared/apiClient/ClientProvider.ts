import {DefaultClient} from "app/shared/apiClient/DefaultClient";
import {apiUrl} from "app/shared/helpers";

const defaultClient: DefaultClient = new DefaultClient(
  apiUrl
);
// example of interceptor
defaultClient.setResponseInterceptors(DefaultClient.onSuccessResponse);

export const ClientProvider = {
  default: defaultClient
};
