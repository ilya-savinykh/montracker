import {BaseClient} from "app/shared/apiClient/BaseClient";
import { AxiosResponse } from "axios";

export class DefaultClient extends BaseClient {
  static onSuccessResponse(res: AxiosResponse): AxiosResponse {
    if ( res.status >= 200 && res.status <= 400 ) {
      return res.data;
    }

    throw new Error(res.data);
  }
}
