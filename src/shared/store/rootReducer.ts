import {combineReducers} from "redux";
import {systemReducer} from "app/shared/store/system";
import {exampleReducer} from "app/domains/example/store";

export const rootReducer = combineReducers({
  system: systemReducer,
  example: exampleReducer
});
