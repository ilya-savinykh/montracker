import {REHYDRATE} from "redux-persist/es/constants";
import {IAppState} from "app/shared/store/appState";
import {actionCreator} from "app/shared/store/actionCreator";

export const rehydrateAction = actionCreator<IAppState>(REHYDRATE);
