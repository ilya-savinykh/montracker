import {ELanguageCode} from "app/shared/helpers";

export interface ISystemState {
  language: ELanguageCode;
}

export const SystemInitialState: ISystemState = {
  language: ELanguageCode.ru
};
