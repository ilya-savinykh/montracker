import {ReducerBuilder, reducerWithInitialState} from "typescript-fsa-reducers";
import {ISystemState, SystemInitialState} from "app/shared/store/system/systemState";
import {ELanguageCode, newState} from "app/shared/helpers";
import {resetAction, setLanguageAction} from "app/shared/store/system/systemActions";

function reset(state: ISystemState): ISystemState {
  return newState(state, {
    ...SystemInitialState,
    language: state.language
  });
}

function setLanguage(state: ISystemState, payload: ELanguageCode): ISystemState {
  return newState(state, {
    language: payload
  });
}

export const systemReducer: ReducerBuilder<ISystemState> = reducerWithInitialState(SystemInitialState)
  .case(resetAction, reset)
  .case(setLanguageAction, setLanguage)
  ;
