import {actionCreator} from "app/shared/store";
import {ELanguageCode} from "app/shared/helpers";

export const resetAction = actionCreator("System/RESET");
export const setLanguageAction = actionCreator<ELanguageCode>("System/SET_LANGUAGE");
