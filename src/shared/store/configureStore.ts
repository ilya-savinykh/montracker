import {applyMiddleware, createStore, Middleware, Store} from "redux";
import logger from "redux-logger";
import {persistReducer, persistStore} from "redux-persist";
import {Persistor} from "redux-persist/es/types";
import {AsyncStorage} from "react-native";
import {rootReducer} from "app/shared/store/rootReducer";
import {IAppState} from "app/shared/store/appState";
import thunk from "redux-thunk";

let middleware: Middleware[] = [thunk];

if (__DEV__) {
  middleware = [...middleware, logger];
} else {
  middleware = [...middleware];
}

const persistConfig = {
  key: "root",
  storage: AsyncStorage,
  whitelist: ["system"],
};
const persistedReducer = persistReducer(persistConfig, rootReducer);

export function configureStore(callback: () => any, initialState?: IAppState): IConfiguredStore {
  const store: Store = createStore(persistedReducer, initialState, applyMiddleware(...middleware));
  const persistor = persistStore(store, undefined, callback);

  return {store, persistor};
}

export interface IConfiguredStore {
  store: Store;
  persistor: Persistor;
}
