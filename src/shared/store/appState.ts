import {ISystemState} from "app/shared/store/system";
import {IExampleState} from "app/domains/example/store";

export interface IAppState {
  system: ISystemState;
  example: IExampleState;
}
