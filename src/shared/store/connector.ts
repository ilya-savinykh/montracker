import {connect} from "react-redux";
import {Dispatch} from "redux";
import {IAppState} from "app/shared/store/appState";
import {IConnectedProps} from "app/shared/views/containers/ConnectedComponent";

export function connector<StateProps, DispatchProps>(
  mapState: ((state: IAppState, ownProps: any) => StateProps) | null,
  mapDispatch?: (dispatch: Dispatch, ownProps: any) => DispatchProps): any {
  return connect(mapState, mapDispatch as any, merge) as any;
}

function merge<IStateProps, IDispatchProps>(
  stateProps: IStateProps,
  dispatchProps: IDispatchProps,
  ownProps: any): IConnectedProps<IStateProps, IDispatchProps> {
  return Object.assign({}, ownProps, {
    stateProps,
    dispatchProps,
  });
}
