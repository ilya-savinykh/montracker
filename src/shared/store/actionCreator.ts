import {actionCreatorFactory} from "typescript-fsa";
import { asyncFactory } from "typescript-fsa-redux-thunk";
import {IAppState} from "app/shared/store/appState";

export const actionCreator = actionCreatorFactory();
export const asyncActionCreator = asyncFactory<IAppState>(actionCreator);
