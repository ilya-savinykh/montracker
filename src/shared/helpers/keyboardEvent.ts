import {KeyboardEventName, Platform} from "react-native";

export const keyboardHideEvent: KeyboardEventName = Platform.OS === "ios" ? "keyboardWillHide" : "keyboardDidHide";
export const keyboardShowEvent: KeyboardEventName = Platform.OS === "ios" ? "keyboardWillShow" : "keyboardDidShow";
