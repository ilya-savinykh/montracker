import {ImageStyle, TextStyle, ViewStyle} from "react-native";
import {platform} from "app/shared/helpers/platform";

export const style = {
  view: (s: ViewStyle): ViewStyle => s,
  text: (s: TextStyle): TextStyle => s,
  image: (s: ImageStyle): ImageStyle => s,
};

export const fonts = {
  medium: platform.is.ios ? "SFUIText-Semibold" : "Roboto-Medium",
  regular: platform.is.ios ? "SFUIText-Regular" : "Roboto-Regular",
  light: "circe-light",
  bold: "circe-bold",
};
