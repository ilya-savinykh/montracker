export enum Color {
  black = "#000000",
  white = "#FFFFFF",
  transparent = "transparent",
  activeTabLine = "#FF7379",
  сobaltBlue = "#162343",
  sapphireBlue = "#101A31",
  oceanBlue = "#183449",
  blackOpacity = "rgba(0,0,0,0.4)",
  purpleBlue = "#3C4D66",
}
