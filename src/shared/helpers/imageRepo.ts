import {ImageURISource} from "react-native";

export class ImageRepo {
  static readonly logo: ImageURISource = require("assets/images/logo.png");
  static readonly tabResults: ImageURISource = require("assets/images/tab_results.png");
  static readonly tabMountain: ImageURISource = require("assets/images/tab_mountain.png");
  static readonly tabEvents: ImageURISource = require("assets/images/tab_events.png");
  static readonly tabSettings: ImageURISource = require("assets/images/tab_settings.png");
  static readonly ok: ImageURISource = require("assets/images/ok.png");
  static readonly close: ImageURISource = require("assets/images/close.png");
}
