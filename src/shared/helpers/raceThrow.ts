export async function raceThrow<T>(p: Promise<T>, timeout: number = 10000, error?: Error): Promise<T | void> {
  const p2 = new Promise<void>((_: any, reject: (e?: Error) => void): void => {
    setTimeout((): void => {
      reject(error);
    }, timeout);
  });

  return Promise.race([p, p2]);
}
