import {Dimensions, StatusBar} from "react-native";
import {isIphoneX as isIphoneXFunc} from "react-native-iphone-x-helper";
import {platform} from "app/shared/helpers/platform";

const windowDimensions = Dimensions.get("window");
export const isIphoneX = isIphoneXFunc();
export const windowWidth = windowDimensions.width;
export const windowHeight = windowDimensions.height - (!platform.is.ios ? StatusBar.currentHeight || 0 : 0);
export const defaultFontSize = windowHeight * .032;
