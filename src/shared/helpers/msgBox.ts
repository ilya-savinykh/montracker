import {Alert} from "react-native";
import {l} from "app/localization";

const alertsQueue: any[] = [];
let alertLocked = false;

export function msgBox(
  text: any, onAccept?: () => any, title: string = l.common.messageBoxHeader
): void {
  if (alertLocked) {
    alertsQueue.push({text, onAccept});

    return;
  }
  alertLocked = true;
  Alert.alert(title, (text || "").toString(), [
    {
      text: "Ok", onPress: (): void => {
        alertLocked = false;
        if (onAccept) {
          onAccept();
        }
        if (alertsQueue.length) {
          const a = alertsQueue.shift();
          msgBox(a.text, a.onAccept);
        }
      },
    },
  ], {cancelable: false});
}
