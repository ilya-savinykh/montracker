import {CommonActions} from "@react-navigation/native";
import {Action} from "@react-navigation/routers/src/CommonActions";

function simpleToRoute(routeName: string): () => Action {
  return (): Action => CommonActions.navigate({name: routeName});
}

function routeWithParams<T>(routeName: string): (params: T) => Action {
  return (params: any): Action => CommonActions.navigate({name: routeName, params});
}

export class Actions {
  navigateToEmpty = simpleToRoute("sEmpty");
  navigateToEmptyWithParams = routeWithParams<{text: string}>("sEmpty");
}
