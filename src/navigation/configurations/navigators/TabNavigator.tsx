import * as React from 'react'
import { Image, ImageURISource, View } from 'react-native'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import {Color, ImageRepo, style, styleSheetCreate, windowHeight, windowWidth} from "app/shared/helpers"
import { InitialScreen } from 'app/domains/example/views/screens/InitialScreen'

const Tab = createBottomTabNavigator()

export const TabNavigator = (): JSX.Element => {
  return (
    <Tab.Navigator
      initialRouteName={"Settings"}
      tabBarOptions={{
        showLabel: false,
        style: styles.tabbarHeight,
      }}
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused }) => {
          let src: ImageURISource;
          switch (route.name) {
            case "Results":
              src = ImageRepo.tabResults
              break
            case "Mountain":
              src = ImageRepo.tabMountain
              break
            case "Events":
              src = ImageRepo.tabEvents
              break
            case "Settings":
              src = ImageRepo.tabSettings
              break
          }
          if (focused) {
            return (
              <View style={styles.activeTab}>
                <Image source={src} resizeMode="contain" style={styles.icon} />
                <View style={styles.activeTabLine} />
              </View>
            );
          } else {
            return <Image source={src} resizeMode="contain" style={styles.icon} />;
          }
        },
      })}
    >
      <Tab.Screen name={"Results"} component={InitialScreen} />
      <Tab.Screen name={"Mountain"} component={InitialScreen} />
      <Tab.Screen name={"Events"} component={InitialScreen} />
      <Tab.Screen name={"Settings"} component={InitialScreen} />
    </Tab.Navigator>
  );
};

const styles = styleSheetCreate({
  tabbarHeight: style.view({
    height: windowWidth * 0.172,
    backgroundColor: Color.сobaltBlue,
    borderTopWidth: 0,
  }),
  icon: style.image({
    width: windowHeight * 0.046,
    height: windowHeight * 0.046,
  }),
  activeTab: style.view({
    height: '100%',
    justifyContent: 'center',
    paddingHorizontal: windowHeight * 0.01,
  }),
  activeTabLine: style.view({
    position: 'absolute',
    bottom: windowHeight * .003,
    width: windowHeight * 0.066,
    height: windowHeight * 0.007,
    backgroundColor: Color.activeTabLine,
  }),
})
