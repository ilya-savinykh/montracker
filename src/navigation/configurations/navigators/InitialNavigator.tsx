import * as React from "react";
import {createStackNavigator} from "@react-navigation/stack";
import {InitialScreen} from "app/domains/example/views/screens/InitialScreen";
import {EmptyScreen} from "app/shared/views/containers/EmptyContainer";

export type TInitialStackParams = {
  sEmpty: {text: string};
  sWelcome: undefined;
};
const Stack = createStackNavigator<TInitialStackParams>();

export const InitialNavigator = (): JSX.Element => {
  return (
    <Stack.Navigator>
      <Stack.Screen name={"sWelcome"} component={InitialScreen}/>
      <Stack.Screen name={"sEmpty"} component={EmptyScreen}/>
    </Stack.Navigator>
  );
};
