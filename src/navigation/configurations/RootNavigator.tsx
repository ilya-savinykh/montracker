import * as React from "react";
import {createStackNavigator, StackNavigationOptions} from "@react-navigation/stack";
import {InitialNavigator} from "app/navigation/configurations/navigators/InitialNavigator";
import { TabNavigator } from "./navigators/TabNavigator";

const Stack = createStackNavigator();

const stackOptions: StackNavigationOptions = {
  header: (): null => null
};

export const RootNavigator = (): JSX.Element => {
  return (
    <Stack.Navigator initialRouteName={"nInitial"}>
      {/* <Stack.Screen
        name={"nInitial"}
        component={InitialNavigator} 
        options={stackOptions}
      /> */}
      <Stack.Screen
        name={"Tabs"}
        component={TabNavigator}
        options={stackOptions}
      />
    </Stack.Navigator>
  );
};
