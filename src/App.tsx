import React, {Component} from "react";
import {Provider} from "react-redux";
import {Persistor} from "redux-persist";
import {PersistGate} from "redux-persist/integration/react";
import {AppState, AppStateStatus} from "react-native";
import {Store} from "redux";
import {NavigationContainer, NavigationContainerRef} from "@react-navigation/native";
import {EventService, NavigationService} from "app/shared/services";
import {configureStore, IAppState} from "app/shared/store";
import {RootNavigator} from "app/navigation/configurations";
import {l} from "app/localization";
import {IEmpty} from "app/shared/helpers";
import {Loader} from "app/shared/views/components/Loader";

interface IState {
  appLoading: boolean;
  navigatedFromStart: boolean;
  appState: AppStateStatus;
}

export class App extends Component<IEmpty, IState> {
  state = {
    appLoading: true,
    navigatedFromStart: false,
    appState: AppState.currentState
  };
  private readonly store: Store;
  private readonly persistor: Persistor;

  constructor(props: IEmpty) {
    super(props);
    const {store, persistor} = configureStore(this.onStoreCreated.bind(this));
    this.store = store;
    this.persistor = persistor;
    this.onAppStateChange = this.onAppStateChange.bind(this);
  }

  componentDidMount(): void {
    AppState.addEventListener("change", this.onAppStateChange);
  }

  componentWillUnmount(): void {
    AppState.addEventListener("change", this.onAppStateChange);
  }

  onAppStateChange(status: AppStateStatus): void {
    if ( this.state.appState.match(/inactive|background/) && status === "active" ) {
      //
    } else if ( this.state.appState === "active" && status.match(/inactive|background/) ) {
      //
    }
    this.setState({appState: status});
  }

  getNavigatorRef(r: NavigationContainerRef): void {
    NavigationService.setTopLevelNavigator(r);
  }

  onStoreCreated(): void {
    const state: IAppState = this.store.getState();
    EventService.init(this.store);
    l.setLanguage(state.system.language);
  }

  renderLoader(): JSX.Element {
    return (
      <Loader
        isLoading={this.state.appLoading}
      />
    );
  }

  render(): JSX.Element {
    return (
      <PersistGate
        persistor={this.persistor}
        loading={this.renderLoader()}
      >
        <Provider
          store={this.store}
        >
          <NavigationContainer ref={this.getNavigatorRef}>
            <RootNavigator/>
          </NavigationContainer>
        </Provider>
      </PersistGate>
    );
  }
}
