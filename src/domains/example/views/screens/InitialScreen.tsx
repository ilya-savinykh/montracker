import React, { PureComponent } from "react";
import { View, Image, ScrollView, SafeAreaView, TextInput, TouchableOpacity } from "react-native";
import {Color, defaultFontSize, fonts, ImageRepo, style, styleSheetCreate, styleSheetFlatten, windowHeight, windowWidth} from "app/shared/helpers";
import {CommonText} from "app/shared/views/components/CommonText";
import {l} from "app/localization";
import {StackNavigationProp} from "@react-navigation/stack";
import { CommonInput } from "app/shared/views/components/CommonInput";

import ModalBox from 'react-native-modalbox'

interface IProps {
  navigation: StackNavigationProp<any>;
}

interface IState {
  openModal: boolean
  date: string
  title: string
  note: string
  time: string
  nameUser: string
  email: string
  company: string
  developers: string
}

export class InitialScreen extends PureComponent<IProps> {

  state: IState

  constructor(props: IProps) {
    super(props);
    this.state = {
      openModal: false,
      date: '23 февраля',
      title: 'Корпоратив',
      note: 'Купить подарки',
      time: '12:00',
      nameUser: 'Екатерина Ермакова',
      email: 'katya123@mail.ru',
      company: 'Microsoft',
      developers: 'Дизайн: OPEN!Design\nИллюстратор: Таня Майфат\nПрограммирование..',
    }
  }

  changeModal = (): void => {
    this.setState({
      openModal: !this.state.openModal
    })
  }

  onChangeDate = (date: string) => {
    this.setState({ date })
  }

  onChangeTitle = (title: string) => {
    this.setState({ title })
  }

  onChangeNote = (note: string) => {
    this.setState({ note })
  }

  onChangeTime = (time: string) => {
    this.setState({ time })
  }

  onChangeNameUser = (nameUser: string) => {
    this.setState({ nameUser })
  }

  onChangeEmail = (email: string) => {
    this.setState({ email })
  }

  onChangeCompany = (company: string) => {
    this.setState({ company })
  }

  onChangeDevelopers = (developers: string) => {
    this.setState({ developers })
  }

  closeModal = (): void => {
    this.setState({ openModal: false })
  }

  renderModal = (): JSX.Element => {
    return (
      <ModalBox
        isOpen={this.state.openModal}
        onClosed={this.closeModal}
        useNativeDriver={true}
        backdropOpacity={0.8}
        style={styles.modal}
      >
        <View style={styles.alignFlexEnd}>
          <TouchableOpacity
            onPress={this.changeModal}
            activeOpacity={0.7}
          >
            <Image
              source={ImageRepo.close}
              style={styles.modalClose}
            />
          </TouchableOpacity>
        </View>
        <ScrollView>
          <CommonInput
            title={l.common.date}
            value={this.state.date}
            onChangeText={this.onChangeDate}
            containerStyle={{marginHorizontal: windowWidth * .08}}
          />
          <CommonInput
            title={l.common.title}
            value={this.state.title}
            onChangeText={this.onChangeTitle}
            containerStyle={{marginHorizontal: windowWidth * .08}}
          />
          <CommonInput
            title={l.common.note}
            value={this.state.note}
            onChangeText={this.onChangeNote}
            containerStyle={{marginHorizontal: windowWidth * .08}}
          />
          <CommonInput
            title={l.common.time}
            value={this.state.time}
            onChangeText={this.onChangeTime}
            containerStyle={{marginHorizontal: windowWidth * .08}}
          />
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.save}>
            <CommonText family={fonts.bold} color={Color.white} size={defaultFontSize * .6} style={{textAlign: 'center'}}>
              {l.common.save}
            </CommonText>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.delete}>
            <CommonText family={fonts.bold} color={Color.white} size={defaultFontSize * .6} style={{textAlign: 'center'}}>
              {l.common.delete}
            </CommonText>
          </TouchableOpacity>
        </ScrollView>
      </ModalBox>
    );
  }

  render(): JSX.Element {
    const inputWrap = styleSheetFlatten([styles.input, style.text({textAlignVertical: 'top', height: windowHeight * .3, lineHeight: windowHeight * .04})])
    return (
      <>
        {this.renderModal()}
        <SafeAreaView style={styles.container}>
          <TouchableOpacity
            onPress={this.changeModal}
            activeOpacity={0.7}
          >
            <Image
              source={ImageRepo.logo}
              style={styles.logo}
            />
          </TouchableOpacity>
          <ScrollView style={styles.content}>
            <CommonInput
              title={l.common.nameUser}
              value={this.state.nameUser}
              onChangeText={this.onChangeNameUser}
            />
            <View style={styles.inputItem}>
              <CommonText family={fonts.light} size={defaultFontSize * .6} color={Color.white} style={styles.inputTitle}>
                {l.common.email}
              </CommonText>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <TextInput
                  value={this.state.email}
                  onChangeText={this.onChangeEmail}
                  style={styles.input}
                />
                <TouchableOpacity activeOpacity={0.7}>
                  <Image
                    source={ImageRepo.close}
                    style={{width: windowWidth * .06, height: windowWidth * .06, marginTop: windowWidth * .035, marginHorizontal: windowWidth * .05}}
                  />
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.7}>
                  <Image
                    source={ImageRepo.ok}
                    style={{width: windowWidth * .06, height: windowWidth * .048, marginTop: windowWidth * .035, marginLeft: windowWidth * .02}}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <CommonInput
              title={l.common.company}
              value={this.state.company}
              onChangeText={this.onChangeCompany}
            />
            <CommonInput
              title={l.common.developers}
              value={this.state.developers}
              onChangeText={this.onChangeDevelopers}
              containerStyle={styles.inputItemWrap}
              inputStyle={inputWrap}
              titleStyle={{fontFamily: fonts.bold}}
              multiline={true}
            />
          </ScrollView>
        </SafeAreaView>
      </>
    );
  }
}

const styles = styleSheetCreate({
  modal: style.view({
    flex: 1,
    backgroundColor: Color.purpleBlue,
    width: windowWidth * .85,
    maxHeight: windowHeight * .8,
    borderRadius: windowWidth * .02
  }),
  alignFlexEnd: style.view({ alignItems: 'flex-end' }),
  modalClose: style.image({
    width: windowWidth * .04,
    height: windowWidth * .04,
    marginTop: windowWidth * .03,
    marginRight: windowWidth * .03
  }),
  container: style.view({
    backgroundColor: Color.sapphireBlue,
    flex: 1,
    alignItems: "center"
  }),
  logo: style.image({
    width: windowWidth * .82,
    height: windowWidth * .236,
    marginVertical: windowHeight * .06,
  }),
  content: style.view({
    width: windowWidth * .82,
  }),
  inputItem: style.view({
    marginTop: windowHeight * .03,
  }),
  inputTitle: style.text({
    marginLeft: windowWidth * .06,
  }),
  input: style.text({
    flex: 1,
    backgroundColor: Color.oceanBlue,
    color: Color.white,
    fontSize: defaultFontSize * .6,
    paddingHorizontal: windowWidth * .06,
    marginTop: windowWidth * .025,
    borderRadius: windowWidth * .0165,
    textDecorationLine: 'none',
    fontFamily: fonts.bold
  }),
  inputItemWrap: style.view({
    marginTop: windowHeight * .08,
    marginBottom: windowHeight * .03,
  }),
  save: style.view({
    backgroundColor: Color.сobaltBlue,
    marginHorizontal: windowWidth * .08,
    marginBottom: windowHeight * .025,
    marginTop: windowHeight * .035,
    paddingVertical: windowHeight * .02,
    borderRadius: windowWidth * .0165
  }),
  delete: style.view({
    backgroundColor: Color.activeTabLine,
    marginHorizontal: windowWidth * .08,
    marginBottom: windowHeight * .035,
    paddingVertical: windowHeight * .02,
    borderRadius: windowWidth * .0165
  }),
});
