import {IExampleResponse} from "app/domains/example/api/dto";

export type TExampleLoading =
  "exampleLoadingType";

export interface IExampleState {
  error: any;
  loading: TExampleLoading | null;
  someData: IExampleResponse | null;
}

export const ExampleInitialState: IExampleState = {
  error: null,
  loading: null,
  someData: null
};
