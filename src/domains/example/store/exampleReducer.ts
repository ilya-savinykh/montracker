import {reducerWithInitialState} from "typescript-fsa-reducers";
import {Failure, Success} from "typescript-fsa";
import {newState} from "app/shared/helpers";
import {ExampleInitialState, IExampleState} from "app/domains/example/store/exampleState";
import {IExampleRequest, IExampleResponse} from "app/domains/example/api/dto";
import {exampleAsyncAction} from "app/domains/example/store/exampleAsyncActions";

function exampleAsyncActionStarted(state: IExampleState): IExampleState {
  return newState(state, { error: null, loading: "exampleLoadingType" });
}

function exampleAsyncActionDone(
  state: IExampleState, payload: Success<IExampleRequest, IExampleResponse>
): IExampleState {
  return newState(state, { error: null, loading: null, someData: payload.result });
}

function exampleAsyncActionFailed(state: IExampleState, failed: Failure<IExampleRequest, Error>): IExampleState {
  return newState(state, { error: failed.error, loading: null });
}

export const exampleReducer = reducerWithInitialState(ExampleInitialState)
  .case(exampleAsyncAction.async.started, exampleAsyncActionStarted)
  .case(exampleAsyncAction.async.done, exampleAsyncActionDone)
  .case(exampleAsyncAction.async.failed, exampleAsyncActionFailed)
;
