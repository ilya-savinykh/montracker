import {asyncActionCreator} from "app/shared/store";
import {IExampleRequest, IExampleResponse} from "app/domains/example/api/dto";
import {exampleApiRequest} from "app/domains/example/api/requests";

export const exampleAsyncAction =
  asyncActionCreator<IExampleRequest, IExampleResponse, Error>(
    "Example/ASYNC_ACTION",
    async (params): Promise<IExampleResponse> => {
      return exampleApiRequest(params);
    });
