import {IExampleRequest, IExampleResponse} from "app/domains/example/api/dto";
import {ClientProvider} from "app/shared/apiClient";

export async function exampleApiRequest(request: IExampleRequest): Promise<IExampleResponse> {
  return ClientProvider.default.post("/login", request);
}
