export interface IExampleRequest {
  email: string;
  password: string;
}

export interface IExampleResponse {
  token: string;
}
